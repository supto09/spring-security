package com.example.secuirity.secuirity;

import com.example.secuirity.model.User;
import com.example.secuirity.repositories.UserRepository;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.List;


/*
 * we need to implement AuthenticationManager so that we can handle user verification manually as default AuthenticationManager
 * uses default UserServiceDetails which is not available here
 * */
public class AuthenticationManagerImpl implements AuthenticationManager {


    private UserRepository userRepository;

    public AuthenticationManagerImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String userName = authentication.getPrincipal().toString();
        String password = authentication.getCredentials().toString();

        User user = userRepository.findByName(userName);

        if (user == null) {
            throw new BadCredentialsException("User not found");
        }
        if (user.getActive() != 1) {
            throw new DisabledException("User is not active");
        }
        if (!password.contentEquals(user.getPassword())) {
            throw new BadCredentialsException("Password mismatch");
        }

        System.out.println(user.toString());


        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        user.getRoles().forEach(role -> grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_" + role.getRole())));

        return new UsernamePasswordAuthenticationToken(userName, password, grantedAuthorities);
    }
}
