package com.example.secuirity.controller;

import com.example.secuirity.model.User;
import com.example.secuirity.repositories.UserRepository;
import com.example.secuirity.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/rest")
public class ApiController {

    private final UserService userService;
    private UserRepository userRepository;

    @Autowired
    public ApiController(UserService userService, UserRepository userRepository) {
        this.userService = userService;
        this.userRepository = userRepository;
    }

    @Secured("ROLE_ADMIN")
    @GetMapping(path = "/all")
    public List<User> root() {
        return userService.getAllUser();
    }

    @Secured("ROLE_USER")
    @GetMapping(path = "/supto")
    public User supto() {
        return userRepository.findByName("supto");
    }

    @GetMapping(path = "/hello")
    public String hello() {
        return "Hello World";
    }

}
